package com.ecem.boluk.focustask.fragment


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.ecem.boluk.focustask.R
import com.ecem.boluk.focustask.adapter.LanguageAdapter
import java.util.*


class LanguageFragment : Fragment() {

    var fragmentView:View? = null
    var LanguageView:RecyclerView? = null
    var langList = mutableListOf<String>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentView = inflater.inflate(R.layout.fragment_language, container, false)
        LanguageView = fragmentView?.findViewById(R.id.langView)

        if(Locale.getDefault().displayLanguage == "Türkçe"){
            langList = mutableListOf("Türkçe","İngilizce")
        }
        else{
            langList = mutableListOf("Turkish","English")
        }


        var langAdapter = LanguageAdapter(langList)
        LanguageView?.layoutManager = LinearLayoutManager(activity,LinearLayoutManager.VERTICAL,false)
        LanguageView?.adapter = langAdapter
        return fragmentView
    }


}
