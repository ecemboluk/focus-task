package com.ecem.boluk.focustask.fragment

import android.app.*
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi

import com.ecem.boluk.focustask.R
import com.ecem.boluk.focustask.helper.DBHelper
import java.util.*
import java.util.concurrent.TimeUnit

class TimeFragment : Fragment() {

    var spinner: Spinner? = null
    var buton: ImageButton? = null
    var offButon: ImageButton? = null
    var progressBar: ProgressBar? = null
    var textCount: TextView? = null
    var fragmentView: View? = null
    val db by lazy { DBHelper(context!!.applicationContext) }


    var isRunning = false
    var timeMil: Long = 0
    var offTimeMil: Long = 0
    var lastTimeMil: Long = 0
    var off:Boolean = true
    var countDownTimer: CountDownTimer? = null
    var n =0

    lateinit var notificationManager: NotificationManager
    lateinit var notificationChannel: NotificationChannel
    lateinit var builder: Notification.Builder

    private var channelId = "com.ecem.boluk.focustask"
    private var description = "Notification Description"

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        fragmentView = inflater.inflate(R.layout.fragment_time, container, false)
        activity?.title = resources.getString(R.string.zamanlayici)
        spinner = fragmentView?.findViewById(R.id.spinner)
        progressBar = fragmentView?.findViewById(R.id.progress_countdown)
        buton = fragmentView?.findViewById(R.id.button)
        offButon = fragmentView?.findViewById(R.id.buttonOff)
        textCount = fragmentView?.findViewById(R.id.count)

        notificationManager =
            context!!.applicationContext.getSystemService(NOTIFICATION_SERVICE) as NotificationManager

        offButon?.isEnabled = false


        var tasks: MutableList<String> = mutableListOf()
        tasks.add(resources.getString(R.string.tasks))
        var list = db.readTask()
        for (i in list) {
            tasks.add(i.task)
        }


        val adapter =
            ArrayAdapter(context!!.applicationContext, R.layout.spinner_textview_align, tasks)
        spinner?.adapter = adapter

        adapter.setDropDownViewResource(R.layout.spinner_textview_align)

        buton?.setOnClickListener {
            if (!isRunning) {
                startCounting()
            } else {
                stopCounting()
            }

        }

        offButon?.setOnClickListener {
            offStartCounting()
        }

        return fragmentView
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    private fun startCounting() {
        if (spinner?.selectedItem.toString() == resources.getString(R.string.tasks)) {
            Toast.makeText(
                context!!.applicationContext,
                "Lütfen bir görev seçin",
                Toast.LENGTH_SHORT
            ).show()
        } else {
            spinner?.isEnabled = false
            if (lastTimeMil == 0.toLong()) {
                timeMil = 10 * 1000
                progressBar?.max = timeMil.toInt() / 1000
            } else if (lastTimeMil >= 1000) {
                timeMil = lastTimeMil
            }
            buton?.setImageResource(R.drawable.ic_pause_circle_filled_black_24dp)
            isRunning = true
            countDownTimer = object : CountDownTimer(timeMil, 1000) {
                override fun onTick(millisUntilFinished: Long) {
                    textCount?.text = timeString(millisUntilFinished)
                    progressBar?.progress = Math.round(millisUntilFinished * 0.001f)
                    lastTimeMil = millisUntilFinished
                    if (lastTimeMil.toInt() < 1000) {
                        createNotification()
                        lastTimeMil = 0
                        updateCompleted(lastTimeMil, spinner?.selectedItemPosition)
                        buton?.setImageResource(R.drawable.ic_play_circle_filled_black_24dp)
                        offButon?.isEnabled = true
                        offTimeMil = 10*1000
                        progressBar?.progress = offTimeMil.toInt() / 1000
                        textCount?.text = "04:00"
                        buton?.isEnabled = false
                    }
                }

                override fun onFinish() {

                }

            }.start()

            countDownTimer!!.start()

        }
    }

    private fun stopCounting() {
        isRunning = false
        buton?.setImageResource(R.drawable.ic_play_circle_filled_black_24dp)
        countDownTimer!!.cancel()
    }

    private fun timeString(millisUntilFinished: Long): String {
        var millisUntilFinished: Long = millisUntilFinished

        val minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)
        millisUntilFinished -= TimeUnit.MINUTES.toMillis(minutes)

        val seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished)


        // Format the string
        return String.format(
            Locale.getDefault(),
            "%02d : %02d ", minutes, seconds
        )
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    private fun createNotification() {

        var intent = Intent(context!!.applicationContext, LauncherActivity::class.java)
        var pendingIntent = PendingIntent.getActivity(
            context!!.applicationContext,
            0,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )

        if(off){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                notificationChannel =
                    NotificationChannel(channelId, description, NotificationManager.IMPORTANCE_HIGH)
                notificationChannel.enableLights(true)
                notificationChannel.lightColor = Color.RED
                notificationChannel.enableVibration(false)
                notificationManager.createNotificationChannel(notificationChannel)

                builder = Notification.Builder(context!!.applicationContext, channelId)
                    .setContentTitle("Focus Task")
                    .setContentText("Görev Başarıyla Tamamlandı.")
                    .setSmallIcon(R.drawable.ic_access_time_black_24dp)
                    .setContentIntent(pendingIntent)
            } else {
                builder = Notification.Builder(context!!.applicationContext)
                    .setContentTitle("Focus Task")
                    .setContentText("Tebrikler, görevi başarıyla tamamladınız.")
                    .setSmallIcon(R.drawable.ic_access_time_black_24dp)
                    .setContentIntent(pendingIntent)
            }
        }
        else if(!off){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                notificationChannel =
                    NotificationChannel(channelId, description, NotificationManager.IMPORTANCE_HIGH)
                notificationChannel.enableLights(true)
                notificationChannel.lightColor = Color.RED
                notificationChannel.enableVibration(false)
                notificationManager.createNotificationChannel(notificationChannel)

                builder = Notification.Builder(context!!.applicationContext, channelId)
                    .setContentTitle("Focus Task")
                    .setContentText("Mola Bitti")
                    .setSmallIcon(R.drawable.ic_access_time_black_24dp)
                    .setContentIntent(pendingIntent)
            } else {
                builder = Notification.Builder(context!!.applicationContext)
                    .setContentTitle("Focus Task")
                    .setContentText("Mola Bitti")
                    .setSmallIcon(R.drawable.ic_access_time_black_24dp)
                    .setContentIntent(pendingIntent)
            }
        }
        notificationManager.notify(1, builder.build())
    }

    private fun updateCompleted(lastTimeMil: Long, position: Int?) {
        if (lastTimeMil == 0.toLong()) {
            var list = db.readTask()
            db.updateCompleted(1, list[position!! - 1].id)
        }
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    private fun offStartCounting(){
        spinner?.isEnabled = false
        offTimeMil = (4*60) * 1000
        progressBar?.max = offTimeMil.toInt() / 1000
        offButon?.isEnabled = false
        countDownTimer = object : CountDownTimer(offTimeMil, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                textCount?.text = timeString(millisUntilFinished)
                progressBar?.progress = Math.round(millisUntilFinished * 0.001f)
                lastTimeMil = millisUntilFinished
                if (lastTimeMil.toInt() < 1000) {
                    lastTimeMil = 0
                    offTimeMil = (25*60)*1000
                    progressBar?.progress = offTimeMil.toInt() / 1000
                    textCount?.text = "25:00"
                    buton?.isEnabled = true
                    spinner?.isEnabled = true
                    off = false
                    createNotification()
                    spinner?.setSelection(0)
                }
            }

            override fun onFinish() {

            }

        }.start()

        countDownTimer!!.start()
    }
}
