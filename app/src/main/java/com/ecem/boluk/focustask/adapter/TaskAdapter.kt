package com.ecem.boluk.focustask.adapter

import Task
import android.content.Context
import android.graphics.Color
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ImageButton
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.ecem.boluk.focustask.R
import com.ecem.boluk.focustask.helper.DBHelper
import kotlinx.android.synthetic.main.task_card.view.*

class TaskAdapter(val taskList: MutableList<Task>,context: Context) : RecyclerView.Adapter<TaskAdapter.ModelViewHolder>() {

    var isFocus : Boolean = false
    var mContext = context
    val db by lazy { DBHelper(context!!.applicationContext)  }

    class ModelViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val editTask: EditText = view.findViewById(R.id.editTask)
        val taskInfo: ImageButton = view.findViewById(R.id.taskInfo)
        var isCompleted: Int = 0

        fun bindItems(item: Task, focus: Boolean) {
            editTask.setText(item.task)
            taskInfo.setColorFilter(Color.BLACK)
            if(focus){
                editTask.requestFocus()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ModelViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.task_card, parent, false)
        return ModelViewHolder(view)
    }

    override fun getItemCount(): Int {
        return taskList.size
    }

    override fun onBindViewHolder(holder: ModelViewHolder, position: Int) {
        holder.bindItems(taskList[position],isFocus)
        holder.isCompleted = taskList[position].isCompleted
        var text = holder.editTask.text

        holder.editTask.setOnEditorActionListener { v, actionId, event ->
            if(actionId == EditorInfo.IME_ACTION_DONE){
                if(text.trim().length == 0){
                    taskList.removeAt(position)
                    notifyItemRemoved(position)
                }
                else if(text.trim().length > 0){
                    if(isFocus){
                        var task = Task()
                        task.task = holder.editTask.text.toString()
                        task.isCompleted = 0
                        db.insertTask(task)
                        isFocus = false
                    }
                    else if(!isFocus){
                        var task = Task()
                        task.task = holder.editTask.text.toString()
                        db.updateTask(task.task,taskList[position].id)
                    }
                }
            }
            false
        }
        holder.editTask.setOnKeyListener { v, keyCode, event ->
            if(keyCode == KeyEvent.KEYCODE_BACK){
                    taskList.removeAt(position)
                    notifyItemRemoved(position)
                true
            }
            false
        }

        holder.taskInfo.setOnClickListener {
            if(taskList[position].isCompleted == 0){
                db.updateCompleted(1,taskList[position].id)
                holder.taskInfo.setColorFilter(ContextCompat.getColor(mContext, R.color.mainColor))
            }
            else if(taskList[position].isCompleted == 1){
                db.updateCompleted(0,taskList[position].id)
                holder.taskInfo.setColorFilter(ContextCompat.getColor(mContext, R.color.black))
            }
        }

        if(taskList[position].isCompleted == 0){
            holder.taskInfo.setColorFilter(ContextCompat.getColor(mContext, R.color.black))
        }
        else if(taskList[position].isCompleted == 1){
            holder.taskInfo.setColorFilter(ContextCompat.getColor(mContext, R.color.mainColor))
        }
    }

    fun createTask(focus: Boolean){
        isFocus = focus
        taskList.add(Task(" ",0))
        notifyItemInserted(taskList.size-1)
    }
    fun removeTask(position: Int){
        db.deleteTask(taskList[position].id)
        taskList.removeAt(position)
        notifyItemRemoved(position)
    }
}