package com.ecem.boluk.focustask.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.ecem.boluk.focustask.R
import com.ecem.boluk.focustask.activity.MainActivity
import com.ecem.boluk.focustask.fragment.LanguageFragment

class SettingsAdapter(var settings:MutableList<String>) : RecyclerView.Adapter<SettingsAdapter.ModelViewHolder>() {

    lateinit var viewF:View
    var isCheched = false

    class ModelViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val settingsText: TextView = view.findViewById(R.id.setting)
        val cardSettings: CardView = view.findViewById(R.id.card)

        fun bindItem(text: String) {
            settingsText.text = text
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ModelViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.settings_card, parent, false)
        viewF = view
        return ModelViewHolder(view)
    }

    override fun getItemCount(): Int {
        return settings.size
    }

    @SuppressLint("ResourceType")
    override fun onBindViewHolder(holder: ModelViewHolder, position: Int) {
        holder.bindItem(settings[position])
        holder.cardSettings.setOnClickListener {
            val activity = viewF.context as MainActivity
            var fragmentTransaction: FragmentTransaction =
                activity.supportFragmentManager.beginTransaction()
            if (position == 0) {
                fragmentTransaction.replace(R.id.activiy_main, LanguageFragment())
                fragmentTransaction.commit()
            }
            else if (position == 1) {
                activity.changeTheme(true)
            }
            else if(position == 2){
                var intent = Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS" )
                activity.startActivity(intent)
            }

        }
    }
}