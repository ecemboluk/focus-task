package com.ecem.boluk.focustask.activity


import android.content.SharedPreferences
import android.content.res.Resources
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceManager
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.ecem.boluk.focustask.R
import com.ecem.boluk.focustask.component.App
import com.ecem.boluk.focustask.fragment.SettingsFragment
import com.ecem.boluk.focustask.fragment.TaskFragment
import com.ecem.boluk.focustask.fragment.TimeFragment
import com.google.android.material.bottomnavigation.BottomNavigationView


class MainActivity : App() {

    lateinit var currentTheme:String
    lateinit var sharedPref:SharedPreferences
    lateinit var navigationView:BottomNavigationView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        currentTheme = sharedPref.getString(KEY_CURRENT_THEME, DEFAULT_THEME).toString()
        callFragment(TimeFragment())
        navigationView = findViewById(R.id.navigatorView)
        navigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        navigationView.selectedItemId = R.id.navigation_time

    }

    fun callFragment(fragment:Fragment){
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.activiy_main,fragment)
        fragmentTransaction.commit()
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_task -> {
                callFragment(TaskFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_time -> {
                callFragment(TimeFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_settings -> {
                callFragment(SettingsFragment())
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }


    fun changeTheme(isChecked:Boolean){
        if (isChecked){
            sharedPref.edit().putString(KEY_CURRENT_THEME, DARK_THEME).apply()
            navigationView.setBackgroundColor(resources.getColor(R.color.black1))
            //navigationView.itemBackground = ContextCompat.getColor(this,R.color.white1)
        }
        else{
            sharedPref.edit().putString(KEY_CURRENT_THEME, DEFAULT_THEME).apply()
            navigationView.setBackgroundColor(resources.getColor(R.color.white1))
        }
        recreate()
    }
}
