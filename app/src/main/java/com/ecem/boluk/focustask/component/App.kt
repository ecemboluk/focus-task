package com.ecem.boluk.focustask.component

import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import androidx.appcompat.app.AppCompatActivity
import com.ecem.boluk.focustask.R


open class App: AppCompatActivity() {

    private lateinit var currentTheme: String
    private lateinit var sharedPref: SharedPreferences
    val KEY_CURRENT_THEME = "current_theme"
    val DARK_THEME = "dark"
    val DEFAULT_THEME = "default"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        currentTheme = sharedPref.getString(KEY_CURRENT_THEME, DEFAULT_THEME).toString()
        setAppTheme(currentTheme)
    }

    override fun onResume() {
        super.onResume()
        val selectedTheme = sharedPref.getString(KEY_CURRENT_THEME, DEFAULT_THEME)
        if(currentTheme != selectedTheme)
            recreate()
    }

    private fun setAppTheme(currentTheme: String) {
        when (currentTheme) {
            DARK_THEME -> setTheme(R.style.AppThemeDark)
            else -> setTheme(R.style.AppTheme)
        }
    }
}