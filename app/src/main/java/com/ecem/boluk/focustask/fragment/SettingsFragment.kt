package com.ecem.boluk.focustask.fragment


import android.os.Bundle
import android.text.Layout
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.ecem.boluk.focustask.R
import com.ecem.boluk.focustask.adapter.SettingsAdapter

/**
 * A simple [Fragment] subclass.
 */
class SettingsFragment : Fragment() {

    var fragmentView: View? = null
    var settingView: RecyclerView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentView = inflater.inflate(R.layout.fragment_settings, container, false)
        activity?.title =resources.getString(R.string.settings)
        settingView = fragmentView?.findViewById(R.id.rwSettings)

        var list = mutableListOf("Dili Değiştir","Karanlık Modu","Bildirimleri Kapat")

        var settingAdapter = SettingsAdapter(list)
        settingView?.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        settingView?.adapter = settingAdapter


        return fragmentView
    }


}
