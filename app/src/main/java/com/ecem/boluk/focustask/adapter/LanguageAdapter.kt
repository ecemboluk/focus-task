package com.ecem.boluk.focustask.adapter

import android.content.res.Configuration
import android.content.res.Resources
import android.os.Build
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.ecem.boluk.focustask.R
import java.util.*

class LanguageAdapter(var lang:MutableList<String>) : RecyclerView.Adapter<LanguageAdapter.ModelViewHolder>(){

    var viewF :View? = null

    class ModelViewHolder(view:View):RecyclerView.ViewHolder(view){
        var lang: TextView = view.findViewById(R.id.lang)

        fun itemLang(item:String){
            lang.text = item
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ModelViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.language_cards,parent,false)
        viewF = view
        return ModelViewHolder(view)
    }

    override fun getItemCount(): Int {
        return lang.size
    }
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onBindViewHolder(holder: ModelViewHolder, position: Int) {
          holder.itemLang(lang[position])
          holder.itemView.setOnClickListener {
              var res: Resources = viewF?.context!!.resources
              var dm: DisplayMetrics = res.displayMetrics
              var conf: Configuration = res.configuration
              if(position == 0){
                  conf.setLocale(Locale("tr"))
              }
              else if(position == 1){
                  conf.setLocale(Locale("en"))
              }
              res.updateConfiguration(conf, dm)
          }
    }

}