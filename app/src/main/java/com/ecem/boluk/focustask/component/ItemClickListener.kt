package com.ecem.boluk.focustask.component

import android.view.View

interface ItemClickListener {
    fun onClick(view: View, position: Int)
}