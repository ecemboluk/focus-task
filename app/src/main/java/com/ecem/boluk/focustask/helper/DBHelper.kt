package com.ecem.boluk.focustask.helper

import Task
import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBHelper(val context: Context) : SQLiteOpenHelper(context,DBHelper.DATABASE_NAME,null,DBHelper.DATABASE_VERSION) {

    private val TABLE_NAME="Tasks"
    private val COL_ID = "id"
    private val COL_NAME = "taskName"
    private val COL_COMPLETED = "completed"

    companion object {
        private val DATABASE_NAME = "TASK_DATABASE"
        private val DATABASE_VERSION = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val createTable = "CREATE TABLE $TABLE_NAME ($COL_ID INTEGER PRIMARY KEY AUTOINCREMENT, $COL_NAME  VARCHAR(256), $COL_COMPLETED  INTEGER)"
        db?.execSQL(createTable)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
    }


    fun insertTask(task:Task){
        val sqliteDB = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(COL_NAME , task.task)
        contentValues.put(COL_COMPLETED, task.isCompleted)
        sqliteDB.insert(TABLE_NAME,null,contentValues)
    }

    fun readTask():MutableList<Task>{
        val taskList = mutableListOf<Task>()
        val sqliteDB = this.readableDatabase
        val query = "SELECT * FROM $TABLE_NAME"
        val result = sqliteDB.rawQuery(query,null)
        if(result.moveToFirst()){
            do {
                val task = Task()
                task.id = result.getInt(result.getColumnIndex(COL_ID))
                task.task = result.getString(result.getColumnIndex(COL_NAME))
                task.isCompleted = result.getInt(result.getColumnIndex(COL_COMPLETED))
                taskList.add(task)
            }while (result.moveToNext())
        }
        result.close()
        sqliteDB.close()
        return taskList
    }

    fun updateTask(task:String,id:Int) {
        val db = this.writableDatabase
        var values = ContentValues()
        values.put(COL_NAME,task)
        db.update(TABLE_NAME,values,COL_ID+"="+id,null)
    }

    fun updateCompleted(completed:Int,id:Int) {
        val db = this.writableDatabase
        var values = ContentValues()
        values.put(COL_COMPLETED,completed)
        db.update(TABLE_NAME,values,COL_ID+"="+id,null)
    }

    fun deleteTask(id:Int){
        val sqliteDB = this.writableDatabase
        sqliteDB.delete(TABLE_NAME,COL_ID + "= '" + id + "';", null)
        sqliteDB.close()
    }
    fun deleteAllTask(){
        val sqliteDB = this.writableDatabase
        sqliteDB.delete(TABLE_NAME,null,null)
        sqliteDB.close()

    }


}