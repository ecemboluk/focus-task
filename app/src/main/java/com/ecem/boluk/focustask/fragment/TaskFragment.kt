package com.ecem.boluk.focustask.fragment


import Task
import android.content.Context
import android.os.Bundle
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageButton
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ecem.boluk.focustask.R
import com.ecem.boluk.focustask.adapter.TaskAdapter
import com.ecem.boluk.focustask.component.SwipeToDeleteCallback
import com.ecem.boluk.focustask.helper.DBHelper


class TaskFragment : Fragment() {

    var fragmentView: View? = null
    var taskView: RecyclerView? = null
    var addTask: ImageButton? =null
    var editTask: EditText? = null
    val db by lazy { DBHelper(context!!.applicationContext)  }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentView =  inflater.inflate(R.layout.fragment_task, container, false)
        taskView = fragmentView?.findViewById(R.id.taskView)
        addTask = fragmentView?.findViewById(R.id.addTask)
        editTask  = fragmentView?.findViewById(R.id.editTask)

        activity?.title = resources.getString(R.string.tasks)

        var list = db.readTask()
        var taskAdapter = TaskAdapter(list,context!!.applicationContext)
        taskView?.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        taskView?.adapter = taskAdapter

        addTask?.setOnClickListener {
            taskAdapter.createTask(true)
            val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, InputMethodManager.HIDE_IMPLICIT_ONLY)
        }

        val swipeHandler = object : SwipeToDeleteCallback(context!!.applicationContext) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val adapter = taskView?.adapter as TaskAdapter
                adapter.removeTask(viewHolder.adapterPosition)
            }
        }

        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(taskView)


        return fragmentView
    }
}
